﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    float rotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rotationSpeed = 15 * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationSpeed, rotationSpeed, rotationSpeed);
    }
}
