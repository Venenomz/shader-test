﻿Shader "Unlit/Raymarch"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

#define maxSteps 100
#define maxDist 100
#define surfaceDist 1e-3 // .001

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

	        struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				
				// ray origin
				float3 ro : TEXCOORD1;
				
				// our hitPoint
				float3 hitPos : TEXCOORD2;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			  

			// our vertex shader
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
				// convert worldspace to object space, scaling camera pos 
				o.ro = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));

				// position in object space
				o.hitPos = v.vertex;

				return o;
			}

			// Get our current distance
			float GetDistance(float3 pos) 
			{
				// draw a sphere at the origin
				float dist = length(pos) - 0.5; // sphere

				// create a torus
				dist = length(float2(length(pos.xz) - 0.5, pos.y)) - 0.1; // torus

				return dist;
			}

			// returns the distance to the scene along the depth of the ray
			float Raymarch(float3 origin, float3 direction)
			{
				// distance we have marched
				float distFromOrigin = 0;

				// track the distance from the scene/surface
				float distanceFromSurface;
				
				// Loop through our ray's steps
				for (int i = 0; i < maxSteps; i++)
				{
					// calculate the current position of the ray
					float3 curRayPos = origin + distFromOrigin * direction;

					// Get the distance from the current ray
					distanceFromSurface = GetDistance(curRayPos);

					// Move the ray further by the shortest distance in the scene
					distFromOrigin += distanceFromSurface;

					// if we dist from surface is smaller than our surface dist, we hit an object
					// Or if our current distance is greater than our max, then we have marched past the object
					if (distanceFromSurface < surfaceDist || distFromOrigin > maxDist)
						break; // So we need to exit our
				}

				return distFromOrigin;
			}

			// Get the normal of our hit point
			float3 GetNormal(float3 hp) 
			{
				// calculate our epsilon 
				float2 epsilon = float2(1e-2, 0);
				
				// our normal is the distance from the hit point with an offset of our epsilon
				float3 n = GetDistance(hp) - float3(
					GetDistance(hp - epsilon.xyy),
					GetDistance(hp - epsilon.yxy),
					GetDistance(hp - epsilon.yyx));

				return normalize(n);
			}

			// shader fragment
			fixed4 frag(v2f i) : SV_Target
			{
				// set the origin of the uv to be in the middle of the face
				float2 uv = i.uv - 0.5;

				// Offset the camera on the z so we can see what's happening
				float3 rayOrigin = i.ro; // float3(0, 0, -3);
				 
				// Set the direction of the ray
				float3 rayDir = normalize(i.hitPos - rayOrigin); // normalize(float3(uv.x, uv.y, 1));

				// Do our ray march
				float dist = Raymarch(rayOrigin, rayDir);

				// our texture
				fixed4 tex = tex2D(_MainTex, i.uv);

				// our colour
				fixed4 colour = 0;

				// mask
				float mask = dot(uv, uv);

				// if our dist is smaller than maxDist than we hit a surface
				if (dist < maxDist) 
				{
					// Get our hit point
					float3 p = rayOrigin + rayDir * dist;
					 
					// Get the normal of that point
					float3 normal = GetNormal(p);

					// Change our colour's rgb 
					colour.rgb = normal;
				}

				// blend our colour and texture
				colour += lerp(colour, tex, smoothstep(0.1, 0.2, mask));

				return colour;
			}
			ENDCG
		}
	}
}
